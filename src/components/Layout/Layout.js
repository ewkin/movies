import React from 'react';
import './Layout.css';
import NavList from "../NavList/NavList";
import Logo from '../../assets/images/Logo.jpg'

const Layout = props => {
    return (
        <>
            <header className="Toolbar">
                <div className="Toolbar-logo">
                    <div className="Logo">
                        <img src={Logo} alt="Movies"/>
                    </div>
                </div>
                <nav>
                    <NavList/>
                </nav>
            </header>
            <main className="Layout-Content">
                {props.children}
            </main>
        </>
    );
};

export default Layout;