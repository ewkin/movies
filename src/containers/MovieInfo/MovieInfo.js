import React, {useEffect, useReducer} from 'react';
import {ADD_SERIES, ADD_TEXT, initialState, reducer} from "../../store/reducer";
import axios from "axios";

const MovieInfo = props => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const {series} = state;

    useEffect(async () => {
        const response = await axios.get('http://api.tvmaze.com/shows/' + props.match.params.id);
        dispatch({type: ADD_SERIES, series: response.data});
    }, []);

    // почему то не получилось обратиться по ключу series.images.medium. Чекните у себя.. хз почему не работает

    return (
        <>
            <div className='Holder'>
                <h1>Search for TV Show: </h1>
                <div className="MovieRequestHolder"><input type="text" value={series.name}/>
                </div>
            </div>
            <div className='Holder'>
                <div className='Img'>
                    <ul>
                        <li>Genres: {series.genres}</li>
                        <li>Premiered: {series.premiered}</li>
                        <li>Status: {series.status}</li>
                        <li>OfficialSite: <a href={series.genres} >follow me</a></li>
                    </ul>

                </div>
                <div className="MovieRequestHolder">
                    <h3>{series.name}</h3>
                    <div dangerouslySetInnerHTML={{__html: (series.summary)}}/>
                    <a href={series.url}>More info</a>
                </div>
            </div>
        </>
    );
};

export default MovieInfo;