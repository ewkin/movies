import React, {useReducer, useEffect, useCallback} from 'react';
import './MovieSearch.css';
import {ADD_SERIES, ADD_TEXT, initialState, reducer} from "../../store/reducer";
import axios from "axios";
import NavItem from "../../components/NavList/NavItem/NavItem";

const MovieSearch = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const {series, text} = state;

    const requestSeries = useCallback(async () => {
        const response = await axios.get('http://api.tvmaze.com/search/shows?q=' + text);
        dispatch({type: ADD_SERIES, series: response.data});
    }, [text]);

    useEffect(() => {
        if (text.length > 3) {
            requestSeries().catch(console.error);
        }
    }, [text, requestSeries]);


    return (
        <div className='Holder'>
            <h1>Search for TV Show: </h1>
            <div className="MovieRequestHolder"><input type="text" onChange={e => {
                dispatch({type: ADD_TEXT, text: e.target.value})
            }}/>
                <ul>
                    {series.map(s => (
                        <NavItem key={s.show.id} to={'/show/' + s.show.id}> {s.show.name} </NavItem>
                    ))}
                </ul>
            </div>
        </div>
    );
};

export default MovieSearch;