export const ADD_TEXT = 'ADD_TEXT';
export const ADD_SERIES = 'ADD_SERIES';


export const initialState = {
    series: [],
    text: ''
};

export const reducer = (state, action) => {
    switch (action.type) {
        case ADD_TEXT:
            return {...state, text: action.text};
        case ADD_SERIES:
            return {...state, series: action.series};
        default:
            return state;
    };
};

