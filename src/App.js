import React from 'react';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import MovieSearch from "./containers/MovieSearch/MovieSearch";
import MovieInfo from "./containers/MovieInfo/MovieInfo";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={MovieSearch} />
                <Route path="/show/:id" component={MovieInfo} />

            </Switch>
        </Layout>
    );
};

export default App;